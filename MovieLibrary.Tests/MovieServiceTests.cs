using Moq;
using MovieLibrary.Core.Models;
using MovieLibrary.Core.Services;
using MovieLibrary.Core.Models.Dto;

namespace MovieLibrary.Tests;

public class MovieServiceTests
{
    private static Movie GetFullMovie(int id)
    {
        return new Movie()
        {
            MovieId = id,
            Category = $"CategoryValue {id}",
            Description = $"DescriptionValue {id}",
            Name = $"NameValue {id}",
            ReleaseYear = 2000 + id,
            Tags = string.Join(";", Enumerable.Range(64 + (id % 8), 4).Select(p => ((char)p).ToString())),
        };
    }

    private static IEnumerable<MovieDto> PartialMovieDtos()
    {
        yield return new MovieDto()
        {
            Name = "Test",
        };
        yield return new MovieDto()
        {
            Category = "Test",
        };
        yield return new MovieDto()
        {
            Description = "Test",
        };
        yield return new MovieDto()
        {
            ReleaseYear = 2001,
        };
        yield return new MovieDto()
        {
            Tags = new[] { "A", "B" }
        };
        yield return new MovieDto()
        {
            Name = "Test",
            Description = "Test",
            ReleaseYear = 2001,
            Tags = new[] { "A", "B" }
        };
        yield return new MovieDto()
        {
            Name = "Test",
            Category = "Test",
            ReleaseYear = 2001,
            Tags = new[] { "A", "B" }
        };
        yield return new MovieDto()
        {
            Name = "Test",
            Category = "Test",
            Description = "Test",
            Tags = new[] { "A", "B" }
        };
        yield return new MovieDto()
        {
            Name = "Test",
            Category = "Test",
            Description = "Test",
            ReleaseYear = 2001,
        };
    }

    [Test]
    public void Service_can_create_movies()
    {
        var newMovie = new MovieDto()
        {
            Name = "Test",
            Category = "Test",
            Description = "Test",
            ReleaseYear = 2005,
            Tags = new[] { "A", "B" }
        };        
        var repo = new Mock<IMovieRepository>();
        repo.Setup(r => r.Create(It.IsAny<Movie>())).Returns(1).Verifiable(Times.Once());

        var service = new MovieService(repo.Object);
        Assert.Multiple(() =>
        {
            Assert.That(service.Create(newMovie).Success, Is.True);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [TestCaseSource(nameof(PartialMovieDtos))]
    public void Create_fails_if_data_is_missing(MovieDto failingDto)
    {
        var repo = new Mock<IMovieRepository>();
        repo.Setup(r => r.Create(It.IsAny<Movie>())).Verifiable(Times.Never());

        var service = new MovieService(repo.Object);
        Assert.Multiple(() =>
        {
            Assert.That(service.Create(failingDto).Success, Is.False);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [Test]
    public void Create_fails_on_repo_error()
    {
        var newMovie = new MovieDto()
        {
            Name = "Test",
            Category = "Test",
            Description = "Test",
            ReleaseYear = 2005,
            Tags = new[] { "A", "B" }
        };
        var repo = new Mock<IMovieRepository>();
        repo.Setup(r => r.Create(It.IsAny<Movie>())).Throws<IOException>().Verifiable(Times.Once());

        var service = new MovieService(repo.Object);
        Assert.Multiple(() =>
        {
            Assert.That(service.Create(newMovie).Success, Is.False);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [Test]
    public void Service_can_update_movies()
    {
        var newData = new MovieDto() { Name = "Test2" };
        var repo = new Mock<IMovieRepository>();
        repo.Setup(r => r.Read(1)).Returns(GetFullMovie(1)).Verifiable(Times.Once());
        repo.Setup(r => r.Update(It.IsAny<Movie>())).Verifiable(Times.Once());

        var service = new MovieService(repo.Object);
        var result = service.Update(1, newData);
        Assert.Multiple(() =>
        {
            Assert.That(result.Success, Is.True);
            Assert.That(result.Message, Is.Null);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [Test]
    public void Service_skips_update_if_no_changes_declared()
    {
        var newData = new MovieDto();
        var repo = new Mock<IMovieRepository>();
        repo.Setup(r => r.Read(1)).Returns(GetFullMovie(1));
        repo.Setup(r => r.Update(It.IsAny<Movie>())).Verifiable(Times.Never());
        var service = new MovieService(repo.Object);
        var result = service.Update(1, newData);
        Assert.Multiple(() =>
        {
            Assert.That(result.Success, Is.True);
            Assert.That(result.Message, Is.Not.Null);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
        repo.Verify();
    }

    [Test]
    public void Service_skips_update_if_no_actual_changes()
    {
        var newData = new MovieDto() { Name = "NameValue 1" };
        var repo = new Mock<IMovieRepository>();
        repo.Setup(r => r.Read(1)).Returns(GetFullMovie(1));
        repo.Setup(r => r.Update(It.IsAny<Movie>())).Verifiable(Times.Never());
        var service = new MovieService(repo.Object);
        var result = service.Update(1, newData);
        Assert.Multiple(() =>
        {
            Assert.That(result.Success, Is.True);
            Assert.That(result.Message, Is.Not.Null);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
        repo.Verify();
    }

    [Test]
    public void Update_fails_if_movie_does_not_exists()
    {
        var newData = new MovieDto() { Name = "Test2" };
        var repo = new Mock<IMovieRepository>();
        var movie = GetFullMovie(1);
        repo.Setup(r => r.Read(1)).Returns((Movie?)null).Verifiable(Times.Once());
        repo.Setup(r => r.Update(movie)).Verifiable(Times.Never());

        var service = new MovieService(repo.Object);
        var result = service.Update(1, newData);
        Assert.Multiple(() =>
        {
            Assert.That(result.Success, Is.False);
            Assert.That(result.Message, Is.Not.Null);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [Test]
    public void Service_can_read_movies()
    {
        var repo = new Mock<IMovieRepository>();
        repo.Setup(r => r.Read(1))
            .Returns(GetFullMovie(1))
            .Verifiable(Times.Once());
        var service = new MovieService(repo.Object);
        var result = service.Read(1);
        Assert.Multiple(() =>
        {
            Assert.That(result.Success, Is.True);
            Assert.That(result.Result, Is.InstanceOf<Movie>());
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [Test]
    public void Read_fails_if_movie_does_not_exist()
    {
        var repo = new Mock<IMovieRepository>();
        repo.Setup(r => r.Read(1))
            .Returns((Movie?)null)
            .Verifiable(Times.Once());
        var service = new MovieService(repo.Object);
        var result = service.Read(1);
        Assert.Multiple(() =>
        {
            Assert.That(result.Success, Is.False);
            Assert.That(result.Message, Is.Not.Null);
            Assert.That(result.Result, Is.Null);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [Test]
    public void Service_can_delete_movies()
    {
        var repo = new Mock<IMovieRepository>();
        repo.Setup(r => r.Read(1)).Returns(GetFullMovie(1)).Verifiable(Times.Once());
        repo.Setup(r => r.Delete(1)).Verifiable(Times.Once());

        var service = new MovieService(repo.Object);
        Assert.Multiple(() =>
        {
            Assert.That(service.Delete(1).Success, Is.True);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [Test]
    public void Querying_by_name_returns_data()
    {
        var repo = new Mock<IMovieRepository>();
        repo.Setup(r => r.GetAll())
            .Returns(Enumerable.Range(1, 10).Select(GetFullMovie).AsQueryable())
            .Verifiable(Times.Exactly(3));
        var service = new MovieService(repo.Object);
        Assert.Multiple(() =>
        {
            Assert.That(service.GetByName("1").ToArray(), Has.Length.EqualTo(2));
            Assert.That(service.GetByName("2").ToArray(), Has.Length.EqualTo(1));
            Assert.That(service.GetByName("XXX").ToArray(), Is.Empty);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [Test]
    public void Querying_by_description_returns_data()
    {
        var repo = new Mock<IMovieRepository>();
        repo.Setup(r => r.GetAll())
            .Returns(Enumerable.Range(1, 10).Select(GetFullMovie).AsQueryable())
            .Verifiable(Times.Exactly(3));
        var service = new MovieService(repo.Object);
        Assert.Multiple(() =>
        {
            Assert.That(service.GetByDescription("1").ToArray(), Has.Length.EqualTo(2));
            Assert.That(service.GetByDescription("2").ToArray(), Has.Length.EqualTo(1));
            Assert.That(service.GetByDescription("XXX").ToArray(), Is.Empty);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }
}