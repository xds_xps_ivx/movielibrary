# Movie Library
This is a test project that includes very basic functionality to list and add movie entities to a database, either using Entity Framework or Dapper.

## Building
MovieLibrary can be built on any platform or CI environment supported by dotnet.

The WebAPI component uses dependency injection to get the required services, and it currently includes support for `MovieLibrary.EfCore` and `MovieLibrary.Dapper`, based on `EntityFrameworkCore` and `Dapper` respectively.

### Prerequisites
- [.Net SDK 6.0](https://dotnet.microsoft.com/).
- [.Net SDK 7.0 or later](https://dotnet.microsoft.com/) (needed for C#11 support).

If trying out the Dapper provider:
- [dotnet-sqlpackage](https://learn.microsoft.com/sql/tools/sqlpackage/sqlpackage-download) (to deploy the database)
- SQL Server 2016 or later (Express or LocalDB will work)

### Building the solution
By default, the solution will build using `MovieLibrary.EfCore`, running with an in-memory database, which might not be ideal for some testing scenarios.

To build using the default `EfCore` provider, run:
```sh
dotnet build ./MovieLibrary.sln
```
To switch defaults, edit the `MovieLibrary.Server.csproj` file, and change the line 14 to:
```xml
	<UseDapper>true</UseDapper>
```
> NOTE: If no default is set and no provider is specified, the solution will fail to build.

#### Using `MovieLibrary.EfCore`
To explicitly use the `EfCore` provider, run:
```sh
dotnet build ./MovieLibrary.sln -p:UseEfCore=true
```
#### Using `MovieLibrary.Dapper`
To explicitly use the `Dapper` provider, run:
```sh
dotnet build ./MovieLibrary.sln -p:UseDapper=true
```
> NOTE: Make sure to have the database defined in the `MovieLibrary.Sql` project deployed, and adjust the connection string accordingly in `appsettings.Development.json`

### Deploying the databse (If using Dapper)
In case you're using Dapper as the repository provider, you'll need to deploy the database at least once. When using `EfCore` this is not required due to it using a database in memory. Moreover, Entity Framework Core can create the DB schema upon service execution by itself without requiring manual database deployments.

Make sure you have the `dotnet-sqlpackage` tool:
```sh
dotnet tool install -g microsoft.sqlpackage
```
Compile the solution with `Dapper`, and use the `dotnet-sqlpackage` tool to deploy the database.
> In this example, we're publishing to a LocalDb instance on the local computer. Adjust the specified connection string as required, and edit the `appsettings.Development.json` file in the `MovieLibrary.Server` project.
```sh
dotnet build ./MovieLibrary.sln -p:UseDapper=true
SqlPackage /Action:Publish /SourceFile:".\MovieLibrary.Sql\bin\Debug\MovieLibrary.Sql.dacpac" /TargetConnectionString:"Server=(localdb)\mssqllocaldb;Database=MovieLibrary;Trusted_Connection=True;"
```
> Note: this is a `PowerShell` command, adjust path characters if using `bash`.

Alternatively, you may deploy the database project using Visual Studio.

### Testing the solution
```sh
dotnet test ./MovieLibrary.sln
```
