﻿CREATE TABLE Movies (
  MovieId int PRIMARY KEY IDENTITY(1,1),
  Name varchar(50) NOT NULL,
  Description varchar(200) NULL,
  Category varchar(50) NULL,
  Tags varchar(100) NULL,
  ReleaseYear int NOT NULL
);