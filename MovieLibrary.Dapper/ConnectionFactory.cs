﻿using System.Data.SqlClient;

namespace MovieLibrary.Dapper;

/// <summary>
/// Class that allows for SQL connection creation for ADO.DB/ADBC/OleDB
/// clients.
/// </summary>
public class ConnectionFactory : IConnectionFactory
{
    private readonly string connectionString;

    /// <summary>
    /// Initializes a new instance of the <see cref="ConnectionFactory"/>
    /// class.
    /// </summary>
    /// <param name="connectionString">
    /// String to use as the connection string.
    /// </param>
    public ConnectionFactory(string connectionString)
    {
        this.connectionString = connectionString;
    }

    /// <inheritdoc/>
    public SqlConnection CreateConnection()
    {
        return new SqlConnection(connectionString);
    }
}
