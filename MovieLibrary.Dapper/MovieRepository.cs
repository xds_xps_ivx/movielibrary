﻿using Dapper;
using MovieLibrary.Core.Models;
using MovieLibrary.Core.Services;
using System.Data.SqlClient;

namespace MovieLibrary.Dapper;

/// <summary>
/// Implements a <see cref="IMovieRepository"/> that uses Dapper for SQL operations.
/// </summary>
public class MovieRepository : IMovieRepository
{
    private readonly SqlConnection connection;
    private bool disposedValue;

    /// <summary>
    /// Initializes a new instance of the <see cref="MovieRepository"/> class.
    /// </summary>
    /// <param name="connectionFactory">
    /// Connection factory instance to use when creating the required SQL
    /// connection.
    /// </param>
    public MovieRepository(IConnectionFactory connectionFactory)
    {
        connection = connectionFactory.CreateConnection();
    }

    /// <inheritdoc/>
    public int Create(Movie movie)
    {
        return Convert.ToInt32((decimal)connection.ExecuteScalar(
            """
            INSERT INTO Movies (Name, Description, Category, Tags, ReleaseYear) VALUES (@Name, @Description, @Category, @Tags, @ReleaseYear);
            SELECT SCOPE_IDENTITY()
            """, movie)!);
    }

    /// <inheritdoc/>
    public Movie Read(int movieId)
    {
        return connection.QueryFirstOrDefault<Movie>("SELECT * FROM Movies WHERE MovieId = @id", new { id = movieId })!;
    }

    /// <inheritdoc/>
    public void Update(Movie data)
    {
        connection.Execute("UPDATE Movies SET Name = @Name, Description = @Description, Category = @Category, Tags = @Tags, ReleaseYear = @ReleaseYear WHERE MovieId = @MovieId", data);
    }

    /// <inheritdoc/>
    public void Delete(int movieId)
    {
        connection.Execute("DELETE FROM Movies WHERE MovieId = @id", new { id = movieId });
    }

    /// <inheritdoc/>
    public IQueryable<Movie> GetAll()
    {
        return connection.Query<Movie>("SELECT * FROM Movies").AsQueryable();
    }

    /// <summary>
    /// Performs the required operations to dispose this instance.
    /// </summary>
    /// <param name="disposing">
    /// Indicates whether or not to dispose internal objects that implement
    /// <see cref="IDisposable"/>.
    /// </param>
    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                connection.Dispose();
            }
            disposedValue = true;
        }
    }

    /// <inheritdoc/>
    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}
