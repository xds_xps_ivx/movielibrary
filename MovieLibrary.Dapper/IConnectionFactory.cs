﻿using System.Data.SqlClient;

namespace MovieLibrary.Dapper;

/// <summary>
/// Defines a set of members to be implemented by a type that provides methods
/// to generate SQL connections.
/// </summary>
public interface IConnectionFactory
{
    /// <summary>
    /// Creates a new <see cref="SqlConnection"/>.
    /// </summary>
    /// <returns>A new <see cref="SqlConnection"/>.</returns>
    SqlConnection CreateConnection();
}
