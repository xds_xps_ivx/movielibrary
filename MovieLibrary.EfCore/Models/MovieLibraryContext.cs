﻿using Microsoft.EntityFrameworkCore;
using MovieLibrary.Core.Models;

namespace MovieLibrary.EfCore.Models;

/// <summary>
/// Defines the data context to be used when interacting with a database.
/// </summary>
public class MovieLibraryContext : DbContext
{
    /// <summary>
    /// Initializes a new instance of the <see cref="MovieLibraryContext"/>
    /// class.
    /// </summary>
    public MovieLibraryContext()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="MovieLibraryContext"/>
    /// class.
    /// </summary>
    /// <param name="options">Options to use when creating the model.</param>
    public MovieLibraryContext(DbContextOptions options) : base(options)
    {
    }

    /// <summary>
    /// Defines a dataset for <see cref="Movie"/> entities.
    /// </summary>
    public DbSet<Movie> Movies { get; set; }
}