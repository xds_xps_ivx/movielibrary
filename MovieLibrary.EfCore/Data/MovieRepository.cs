﻿using MovieLibrary.Core.Models;
using MovieLibrary.Core.Services;
using MovieLibrary.EfCore.Models;

namespace MovieLibrary.EfCore.Data;

/// <summary>
/// Implements a <see cref="IMovieRepository"/> that uses Entity Framework to
/// perform operations against a database.
/// </summary>
public class MovieRepository : IMovieRepository
{
    private readonly MovieLibraryContext context;
    private bool disposedValue;

    /// <summary>
    /// Initializes a new instance of the <see cref="MovieRepository"/> class.
    /// </summary>
    /// <param name="context">Context to perform data operations onto.</param>
    public MovieRepository(MovieLibraryContext context)
    {
        context.Database.EnsureCreated();
        this.context = context;
    }

    /// <inheritdoc/>
    public int Create(Movie movie)
    {
        context.Movies.Add(movie);
        context.SaveChanges();
        return movie.MovieId;
    }

    /// <inheritdoc/>
    public void Delete(int movieId)
    {
        if (context.Movies.Find(movieId) is { } movie)
        {
            context.Movies.Remove(movie);
            context.SaveChanges();
        }
    }

    /// <inheritdoc/>
    public IQueryable<Movie> GetAll()
    {
        return context.Movies;
    }

    /// <inheritdoc/>
    public void Update(Movie data)
    {
        context.Update(data);
        context.SaveChanges();
    }

    /// <summary>
    /// Performs the required operations to dispose this instance.
    /// </summary>
    /// <param name="disposing">
    /// Indicates whether or not to dispose internal objects that implement
    /// <see cref="IDisposable"/>.
    /// </param>
    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                context.Dispose();
            }
            disposedValue = true;
        }
    }

    /// <inheritdoc/>
    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}
