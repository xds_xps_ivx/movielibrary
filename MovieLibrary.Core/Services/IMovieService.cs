﻿using MovieLibrary.Core.Models;
using MovieLibrary.Core.Models.Dto;

namespace MovieLibrary.Core.Services;

/// <summary>
/// Defines a set of members to be implemented by a type that can store and
/// retrieve movies.
/// </summary>
public interface IMovieService
{
    /// <summary>
    /// Creates a new movie.
    /// </summary>
    /// <param name="movie">Movie DTO with the data to insert.</param>
    /// <returns>
    /// A <see cref="ServiceResult{T}"/> with the movie id of the newly
    /// created movie.
    /// </returns>
    public ServiceResult<int> Create(MovieDto movie);

    /// <summary>
    /// Gets an existing movie with the specified id.
    /// </summary>
    /// <param name="movieId">Movie Id to get.</param>
    /// <returns>
    /// A <see cref="ServiceResult{T}"/> with either the movie that has the
    /// specified movie id or <see langword="null"/> if no such movie has
    /// been found.
    /// </returns>
    public ServiceResult<Movie> Read(int movieId);

    /// <summary>
    /// Updates an existing movie with the specified data.
    /// </summary>
    /// <param name="movieId">Movie Id to update.</param>
    /// <param name="data">DTO with the new data to be set.</param>
    /// <returns>
    /// A <see cref="ServiceResult"/> that indicates either success or failure
    /// to perform the operation.
    /// </returns>
    public ServiceResult Update(int movieId, MovieDto data);

    /// <summary>
    /// Deletes the movie with the specified movie id.
    /// </summary>
    /// <param name="movieId">Movie Id to delete.</param>
    /// <returns>
    /// A <see cref="ServiceResult"/> that indicates either success or failure
    /// to perform the operation.
    /// </returns>
    public ServiceResult Delete(int movieId);

    /// <summary>
    /// Gets all movies that contain the specified string in its name.
    /// </summary>
    /// <param name="name">Name to search for.</param>
    /// <returns>
    /// A collection of all movies whose names include the specified string.
    /// </returns>
    IEnumerable<Movie> GetByName(string name);

    /// <summary>
    /// Gets all movies that contain the specified string in its description.
    /// </summary>
    /// <param name="description">Description to search for.</param>
    /// <returns>
    /// A collection of all movies whose descriptions include the specified
    /// string.
    /// </returns>
    IEnumerable<Movie> GetByDescription(string description);

    /// <summary>
    /// Enumerates all movies.
    /// </summary>
    /// <returns>A collection of all movies</returns>
    IEnumerable<Movie> GetAll();
}
