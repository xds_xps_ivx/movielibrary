﻿using MovieLibrary.Core.Models;

namespace MovieLibrary.Core.Services;

/// <summary>
/// Defines a set of members to be implemented by a type that exposes methods
/// to read and write movies in a data repository.
/// </summary>
public interface IMovieRepository : IDisposable
{
    /// <summary>
    /// Adds the movie into the repository data store.
    /// </summary>
    /// <param name="movie">Movie to add.</param>
    /// <returns>The ID of the newly created movie.</returns>
    public int Create(Movie movie);

    /// <summary>
    /// Reads a movie with the specified Id from the data store.
    /// </summary>
    /// <param name="movieId">movie Id to read.</param>
    /// <returns>
    /// A movie with the specified Id, or <see langword="null"/> if no such
    /// movie exists.
    /// </returns>
    public Movie? Read(int movieId) => GetAll().SingleOrDefault(p => p.MovieId == movieId);

    /// <summary>
    /// Updates a movie already present in the data store.
    /// </summary>
    /// <param name="data">
    /// Data to be set onto an existing entity in the data store.
    /// </param>
    public void Update(Movie data);

    /// <summary>
    /// Deletes a movie with the specified Id from the data store.
    /// </summary>
    /// <param name="movieId">Id of the movie to be deleted.</param>
    public void Delete(int movieId);

    /// <summary>
    /// Gets a queryable object that may be used to construct queries into the
    /// data store.
    /// </summary>
    /// <returns>
    /// A queryable object that may be used to construct queries into the data
    /// store.
    /// </returns>
    public IQueryable<Movie> GetAll();
}