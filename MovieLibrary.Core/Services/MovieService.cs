﻿using MovieLibrary.Core.Models;
using MovieLibrary.Core.Models.Dto;

namespace MovieLibrary.Core.Services;

/// <summary>
/// Implements a movie service that reads and writes data in an
/// <see cref="IMovieRepository"/> instance.
/// </summary>
public class MovieService : IMovieService
{
    /// <summary>
    /// Contains a collection of validations to be performed on POST request to
    /// create new movies. Applied against <see cref="MovieDto"/> objects.
    /// </summary>
    /// <remarks>
    /// For failing validations, the methods must return
    /// <see langword="true"/>, whereas for passing validations they must
    /// return <see langword="false"/>.
    /// </remarks>
    private static readonly Func<MovieDto, bool>[] postContracts =
    {
        p => p is null,
        p => string.IsNullOrWhiteSpace(p.Name),
        p => string.IsNullOrWhiteSpace(p.Description),
        p => string.IsNullOrWhiteSpace(p.Category),
        p => p.Tags is null,
        p => p.ReleaseYear is null,
    };
    private readonly IMovieRepository repository;

    /// <summary>
    /// Initializes a new instance of the <see cref="MovieService"/> class.
    /// </summary>
    /// <param name="repository">
    /// Repository instance to use when retrieving and/or saving data.
    /// </param>
    public MovieService(IMovieRepository repository)
    {
        this.repository = repository;
    }

    /// <inheritdoc/>
    public ServiceResult<int> Create(MovieDto movie)
    {
        if (postContracts.Any(p => p.Invoke(movie)))
        {
            return new ServiceResult<int>(false, "The request to create a new movie is missing some data.");
        }
        Movie newMovie = new()
        {
            Name = movie.Name!,
            Description = movie.Description!,
            Category = movie.Category!,
            Tags = JoinArray(movie.Tags),
            ReleaseYear = movie.ReleaseYear!.Value
        };
        try
        {
            var result = repository.Create(newMovie);
            return result > 0
                ? result
                : new ServiceResult<int>(false, "Error while creating the new movie.");
        }
        catch (Exception ex)
        {
            return new ServiceResult<int>(false, ex.Message);
        }
    }

    /// <inheritdoc/>
    public ServiceResult Delete(int movieId)
    {
        if (Read(movieId) is { Success: true })
        {
            repository.Delete(movieId);
            return true;
        }
        else
        {
            return new ServiceResult(false, "Movie not found");
        };
    }

    /// <inheritdoc/>
    public ServiceResult<Movie> Read(int movieId)
    {
        return repository.Read(movieId) is { } movie
            ? movie
            : new ServiceResult<Movie>(false, "Movie not found");
    }

    /// <inheritdoc/>
    public ServiceResult Update(int movieId, MovieDto data)
    {
        if (Read(movieId) is { Success: true, Result: { } updatedMovie })
        {
            bool changesMade = false;
            void SetIfNotNull<T>(T? value, Func<Movie, T> currentValue, Action<Movie, T> setter) where T : notnull
            {
                if (value is not null && !value.Equals(currentValue.Invoke(updatedMovie)))
                { 
                    changesMade = true;
                    setter.Invoke(updatedMovie, value);
                }
            }

#pragma warning disable CS8714
            SetIfNotNull(data.Name, p => p.Name, (p, v) => p.Name = v);
            SetIfNotNull(data.Description, p => p.Description, (p, v) => p.Description = v);
            SetIfNotNull(data.Category, p => p.Category, (p, v) => p.Category = v);
            SetIfNotNull(data.ReleaseYear, p => p.ReleaseYear, (p, v) => p.ReleaseYear = v!.Value);
#pragma warning restore CS8714

            if (data.Tags is not null && data.Tags.Length > 0)
            {
                changesMade = true;
                updatedMovie.Tags = JoinArray(data.Tags);
            }

            if (changesMade)
            {
                repository.Update(updatedMovie);
            }
            return new ServiceResult(true, changesMade ? null : "No changes were made");
        }
        else
        { 
            return new ServiceResult(false, "Movie not found");
        };
    }

    /// <inheritdoc/>
    public IEnumerable<Movie> GetByName(string name)
    {
        return repository
            .GetAll()
            .Where(p => p.Name.Contains(name, StringComparison.InvariantCultureIgnoreCase));
    }

    /// <inheritdoc/>
    public IEnumerable<Movie> GetByDescription(string description)
    {
        return repository
            .GetAll()
            .Where(p => p.Description.Contains(description, StringComparison.InvariantCultureIgnoreCase));
    }

    /// <inheritdoc/>
    public IEnumerable<Movie> GetAll()
    {
        return repository.GetAll();
    }

    private static string JoinArray(string[]? array)
    {
        return array is not null ? string.Join(";", array) : string.Empty;
    }
}
