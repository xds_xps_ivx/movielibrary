﻿namespace MovieLibrary.Core.Models.Dto;

/// <summary>
/// Defines a movie DTO that may be used to perform Create/Update operations.
/// </summary>
public class MovieDto
{
    /// <summary>
    /// Gets or sets the movie name.
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// Gets or sets the movie description.
    /// </summary>
    public string? Description { get; set; }

    /// <summary>
    /// Gets or sets the movie category.
    /// </summary>
    public string? Category { get; set; }

    /// <summary>
    /// Gets or sets the movie tags as a semicolon-separated string.
    /// </summary>
    public string[]? Tags { get; set; }

    /// <summary>
    /// Gets or sets the movie release year.
    /// </summary>
    public int? ReleaseYear { get; set; }
}
