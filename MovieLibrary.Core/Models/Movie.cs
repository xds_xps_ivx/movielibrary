﻿namespace MovieLibrary.Core.Models;

/// <summary>
/// Defines a movie.
/// </summary>
public class Movie
{
    /// <summary>
    /// Gets or sets the movie id.
    /// </summary>
    public int MovieId { get; set; }

    /// <summary>
    /// Gets or sets the movie name.
    /// </summary>
    public string Name { get; set; } = null!;

    /// <summary>
    /// Gets or sets the movie description.
    /// </summary>
    public string Description { get; set; } = null!;

    /// <summary>
    /// Gets or sets the movie category.
    /// </summary>
    public string Category { get; set; } = null!;

    /// <summary>
    /// Gets or sets the movie tags as a semicolon-separated string.
    /// </summary>
    public string Tags { get; set; } = null!;

    /// <summary>
    /// Gets or sets the movie release year.
    /// </summary>
    public int ReleaseYear { get; set; }
}

