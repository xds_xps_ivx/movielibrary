﻿using MovieLibrary.Core.Services;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Reflection;
using Microsoft.OpenApi.Models;

#if UseEfCore
using Microsoft.EntityFrameworkCore;
using MovieLibrary.EfCore.Data;
using MovieLibrary.EfCore.Models;
#elif UseDapper
using MovieLibrary.Dapper;
#endif

namespace MovieLibrary.Server.Properties;

internal static class WebAppBuilder
{
    public static WebApplication BuildWebApp()
    {
        var app = ConfigureServices().Build();

        app.UseDefaultFiles();
        app.UseStaticFiles();

        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }
        app.UseAuthentication();
        app.UseRouting();
        app.UseAuthorization();
        app.MapControllers();
        app.MapFallbackToFile("/index.html");
        app.UseCors(x => x
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .SetIsOriginAllowed(origin => true)
                    .AllowCredentials());
        return app;
    }

    private static WebApplicationBuilder ConfigureServices()
    {
        var builder = WebApplication.CreateBuilder(Environment.GetCommandLineArgs());
        var connectionString = builder.Configuration.GetConnectionString("MovieLibrary");
#if UseEfCore
        builder.Services.AddDbContext<MovieLibraryContext>(options => options.UseInMemoryDatabase(nameof(MovieLibraryContext)));
#elif UseDapper
        builder.Services.AddSingleton<IConnectionFactory>(new ConnectionFactory(connectionString));        
#endif
        builder.Services.AddTransient<IMovieRepository, MovieRepository>();
        builder.Services.AddTransient<IMovieService, MovieService>();
        builder.Services.AddControllers();
        builder.Services.AddEndpointsApiExplorer();
#if IncludeAuthorization
        builder.Services.AddAuthentication("Bearer").AddJwtBearer().AddJwtBearer("LocalAuthIssuer");
#endif
        builder.Services.AddSwaggerGen(SetupSwagger);

        return builder;
    }

    private static void SetupSwagger(SwaggerGenOptions options)
    {
        options.SwaggerDoc("v1", new OpenApiInfo
        {
            Version = "v1",
            Title = "Movie Library API",
            Description = "An ASP.NET Core Web API for managing Movies",
        });
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
    }
}
