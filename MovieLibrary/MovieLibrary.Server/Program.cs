using MovieLibrary.Server.Properties;

namespace MovieLibrary.Server;

/// <summary>
/// Defines the main entry point of the application.
/// </summary>
public class Program
{
    /// <summary>
    /// Entry point of the application.
    /// </summary>
    /// <returns>
    /// A <see cref="Task"/> that can be used to await the async operation.
    /// </returns>
    public static Task Main()
    {
        var app = WebAppBuilder.BuildWebApp();
        TestFill.PopulateTestDb(5, app.Configuration);
        return app.RunAsync();
    }
}
