using Microsoft.AspNetCore.Mvc;
using MovieLibrary.Core.Models;
using MovieLibrary.Core.Services;
using MovieDto = MovieLibrary.Core.Models.Dto.MovieDto;

#if IncludeAuthorization
using Microsoft.AspNetCore.Authorization;
#endif

namespace MovieLibrary.Api.Controllers;

/// <summary>
/// Defines a controller to manage a movies repository.
/// </summary>
[ApiController]
[Route("[controller]")]
#if IncludeAuthorization
[Authorize]
#endif
public class MoviesController : ControllerBase
{
    private readonly IMovieService service;

    /// <summary>
    /// Initializes a new instance of the <see cref="MoviesController"/> class.
    /// </summary>
    /// <param name="service">
    /// <see cref="IMovieService"/> dependency to be injected into this
    /// instance.
    /// </param>
    public MoviesController(IMovieService service)
    {
        this.service = service;
    }

    /// <summary>
    /// Creates a new movie entry.
    /// </summary>
    /// <param name="movie">
    /// Data of the new movie to be created.
    /// </param>
    /// <returns>The Id of the newly created movie.</returns>
    /// <response code="201">
    /// Returns the id of th newly created movie.
    /// </response>
    /// <response code="400">
    /// Returned if the movie could not be created due to incomplete data.
    /// </response>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(int))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public ActionResult Post([FromBody] MovieDto movie)
    {
        return service.Create(movie) is { Success: true } result
            ? Created($"1", result.Result)
            : BadRequest();
    }

    /// <summary>
    /// Updates an existing movie.
    /// </summary>
    /// <param name="movieId">Id of the movie to be updated.</param>
    /// <param name="movie">Data to update onto the movie.</param>
    /// <returns>
    /// A response that rescribes either success or failure to perform the
    /// operation.
    /// </returns>
    /// <response code="204">
    /// No data is returned.
    /// </response>
    /// <response code="400">
    /// Returned if the movie could not be updated due to invalid data.
    /// </response>
    [HttpPatch("{movieId}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public ActionResult Patch(int movieId, [FromBody] MovieDto movie)
    {
        var readResult = service.Read(movieId);
        if (!readResult.Success) return NotFound();

        return service.Update(movieId, movie) is { Success: true }
            ? Ok()
            : BadRequest();
    }

    /// <summary>
    /// Searches movies either by name or description.
    /// </summary>
    /// <param name="name">Name query.</param>
    /// <param name="description">Description query.</param>
    /// <returns>
    /// An array of all movies that match the specified query.
    /// </returns>
    /// <remarks>
    /// If both <paramref name="name"/> and <paramref name="description"/> are
    /// specified, the <paramref name="name"/> query takes precedence and
    /// <paramref name="description"/> is ignored.
    /// </remarks>
    /// <response code="200">
    /// Returns an array of movies that match the specified query is returned.
    /// </response>
    /// <response code="400">
    /// Returned if no valid query parameters are specified.
    /// </response>
    [HttpGet("Search")]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public ActionResult<Movie[]> Search([FromQuery] string? name, [FromQuery] string? description)
    {
        if (name is not null)
        {
            return service.GetByName(name).ToArray();
        }
        if (description is not null)
        {
            return service.GetByDescription(description).ToArray();
        }
        return BadRequest();
    }

    /// <summary>
    /// Gets a paged array of all movies.
    /// </summary>
    /// <param name="page">Page number.</param>
    /// <param name="itemsPerPage">Items per page to get.</param>
    /// <returns>
    /// An array of all movies in the specified page number.
    /// </returns>
    /// <response code="200">
    /// Returns an array of movies is returned.
    /// </response>
    [HttpGet("All")]
    [Produces("application/json")]
    public ActionResult<Movie[]> All([FromQuery] int? page, [FromQuery] int? itemsPerPage)
    {
        if (page.HasValue || itemsPerPage.HasValue)
        {
            return service.GetAll().Skip(((page ?? 1) - 1) * (itemsPerPage ?? 10)).Take(itemsPerPage ?? 10).ToArray();
        }
        else
        {
            return service.GetAll().ToArray();
        }
    }

    /// <summary>
    /// Gets a count of pages based on the <paramref name="itemsPerPage"/>
    /// query.
    /// </summary>
    /// <param name="itemsPerPage">Number of items per page.</param>
    /// <returns>
    /// The number of pages based on how many items per page should be
    /// displayed.
    /// </returns>
    /// <response code="200">
    /// Returns the number of pages that would exist if displaying the
    /// specified number of items per page.
    /// </response>
    /// <response code="400">
    /// Returned if itemsPerPage is either not specified or less than 1.
    /// </response>
    [HttpGet("NumberOfPages")]
    [Produces("text/plain", Type = typeof(int))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public ActionResult<int> NumberOfPages([FromQuery] int itemsPerPage)
    {
        if (itemsPerPage <= 0) return BadRequest();
        var c = service.GetAll().Count();
        return (c / itemsPerPage) + (c % itemsPerPage != 0 ? 1 : 0);
    }

    /// <summary>
    /// Reads a single movie item.
    /// </summary>
    /// <param name="movieId">Id of the movie to read.</param>
    /// <returns>A movie from the repository.</returns>
    /// <response code="200">
    /// Returns a movie.
    /// </response>
    /// <response code="404">
    /// Returned if a movie with the specified id does not exist.
    /// </response>
    [HttpGet("{movieId}")]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public ActionResult<Movie> Get(int movieId)
    {
        return service.Read(movieId) is { Success: true, Result: { } movie } ? new ActionResult<Movie>(movie) : NotFound();
    }

    /// <summary>
    /// Deletes a movie from the repository.
    /// </summary>
    /// <param name="movieId">Id of the movie to be deleted.</param>
    /// <returns>
    /// A result that indicates either success or failure to perform the
    /// operation.
    /// </returns>
    /// <response code="204">
    /// No data is returned.
    /// </response>
    /// <response code="404">
    /// Returned if a movie with the specified id does not exist.
    /// </response>
    [HttpDelete("{movieId}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public ActionResult Delete(int movieId)
    {
        return service.Delete(movieId) is { Success: true } ? Ok() : NotFound();
    }
}