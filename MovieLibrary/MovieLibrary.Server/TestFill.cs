﻿using MovieLibrary.Core.Models;
using MovieLibrary.Core.Services;

#if UseEfCore
using Microsoft.EntityFrameworkCore;
using MovieLibrary.EfCore.Data;
using MovieLibrary.EfCore.Models;
#elif UseDapper
using MovieLibrary.Dapper;
#endif

namespace MovieLibrary.Server;

/// <summary>
/// Class that allows for data seeding on a test or empty database.
/// </summary>
/// <remarks>For testing purposes only.</remarks>
public static class TestFill
{
    /// <summary>
    /// Populates the database with random test data.
    /// </summary>
    /// <param name="qty">Amount of entries to generate.</param>
    /// <param name="configuration">
    /// Configuration instance to use when getting the required connection
    /// string.
    /// </param>
    public static void PopulateTestDb(int qty, IConfiguration configuration)
    {
        using var svc = GetRepository(configuration);
        if (!svc.GetAll().Any())
        {
            foreach (var movie in Enumerable.Range(1, qty).Select(GetFullMovie))
            {
                svc.Create(movie);
            }
        }
    }

    private static IMovieRepository GetRepository(IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString("MovieLibrary");
#if UseEfCore
        var options = new DbContextOptionsBuilder();
        options.UseInMemoryDatabase(nameof(MovieLibraryContext));
        var context = new MovieLibraryContext(options.Options);
        return new MovieRepository(context);
#elif UseDapper
        var f = new ConnectionFactory(connectionString);
        return new MovieRepository(f);
#endif
    }

    private static Movie GetFullMovie(int id)
    {
        return new Movie()
        {
            MovieId = id,
            Category = $"Category {((id - 1) % 4) + 1}",
            Description = $"Awesome Movie {id} is awesome. Lorem ipsum dolot sit amet consectur viamos laude.",
            ReleaseYear = 2002 + (id % 4),
            Name = $"Awesome Movie {id}",
            Tags = string.Join(";", Enumerable.Range(65 + ((id - 1) % 8), 4).Select(p => ((char)p).ToString())),
        };
    }
}