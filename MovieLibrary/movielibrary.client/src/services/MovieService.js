export function getDefaultMovieFormData() {
    return { name: "", description: "", category: "", tags: [], releaseYear: new Date().getUTCFullYear() }
}
export async function createMovie(newMovie) {
    try {
        return await fetch("http://localhost:5265/movies", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(newMovie),
        })
    } catch (e) {
        console.log(e)
    }
}
