import { useState } from 'react';
import './App.css';
import { getDefaultMovieFormData, createMovie } from './services/MovieService.js'

function NewMovieForm({ movies, setMovies }) {
    const [newItem, setNewItem] = useState(getDefaultMovieFormData());

    async function createNewMovie() {
        const r = await createMovie(newItem)
        if (r.ok) {
            setMovies([...movies, { ...newItem, id: await r.text() }]);
            setNewItem(getDefaultMovieFormData());
        }
    }

    return (<div className="form">
        <h2>Add new movie</h2>
        <div>
            <div className="form-grid">
                <label htmlFor="name">Name:</label>
                <input
                    type="text"
                    id="name"
                    onChange={(e) => { setNewItem({ ...newItem, name: e.target.value }) }}
                    value={newItem.name} />
            </div>
            <div className="form-grid">
                <label htmlFor="description">Description:</label>
                <input
                    type="text"
                    id="description"
                    onChange={(e) => { setNewItem({ ...newItem, description: e.target.value }) }}
                    value={newItem.description} />
            </div>
            <div className="form-grid">
                <label>Category:</label>
                <input
                    type="text"
                    id="category"
                    onChange={(e) => { setNewItem({ ...newItem, category: e.target.value }) }}
                    value={newItem.category} />
            </div>
            <div className="form-grid">
                <label>Release year:</label>
                <input
                    type="number"
                    id="release-year"
                    onChange={(e) => { setNewItem({ ...newItem, releaseYear: e.target.value }) }}
                    value={newItem.releaseYear} />
            </div>
            <div className="form-grid">
                <label>Tags:</label>
                <input
                    type="text"
                    id="tags"
                    onChange={(e) => { setNewItem({ ...newItem, tags: e.target.value.split(';') }) }}
                    value={newItem.tags.join(';')} />
            </div>
        </div>
        <button onClick={createNewMovie}>Create</button>
    </div>)
}

export default NewMovieForm;