import './App.css';

function MovieTable({ movies }) {
    return (<table className="table table-striped" aria-labelledby="tabelLabel">
        <thead>
            <tr>
                <th>Name</th>
                <th>Release year</th>
                <th>Category</th>
                <th>Description</th>
                <th>Tags</th>
            </tr>
        </thead>
        <tbody>
            {movies.map((movie) =>
                <tr key={movie.name}>
                    <td>{movie.name}</td>
                    <td>{movie.releaseYear}</td>
                    <td>{movie.category}</td>
                    <td>{movie.description}</td>
                    <td>{movie.tags}</td>
                </tr>
            )}
        </tbody>
    </table>)
}

export default MovieTable;