import { useEffect, useState } from 'react';
import './App.css';
import NewMovieForm from './NewMovieForm';
import MovieTable from './MovieTable';

function App() {
    const [errorMessage, setErrorMessage] = useState("");
    const [movies, setMovies] = useState([]);
    const [filter, setFilter] = useState("");
    const handleSearch = (event) => {
        const filter = event.target.value;
        setFilter(filter);
    };
    const filterItems = () => {
        if (filter === "") {
            return movies;
        }
        else {
            return movies.filter(
                (item) =>
                    item.name.toLowerCase().includes(filter.toLowerCase()) ||
                    item.description.toLowerCase().includes(filter.toLowerCase())
            );
        }
    };
    const fetchMovies = (query) => {
        setErrorMessage("Loading...")
        fetch(`http://localhost:5265/movies/${query}`)
            .then(r => {
                r.json().then(j => {
                    setErrorMessage("")
                    setMovies(j)
                });
            }).catch(r => {
                setErrorMessage(`Error fetching movie list. (Bad CORS policy, or server not running?)\n${r}`)
            })
    }
    const updateMovies = (movies) => {
        setMovies(movies)
    }
    const contents = movies === undefined || movies.length === 0 || errorMessage != ""
        ? <p><em>{errorMessage}</em></p>
        : <div className="app-grid">
            <div>
                <h2>Search</h2>
                <div className="search form-grid">
                    <label htmlFor="filter">Filter:</label>
                    <input type="text" id="filter" value={filter} onChange={handleSearch} />
                </div>
                <MovieTable movies={filterItems()} />
            </div>
            <NewMovieForm movies={movies} setMovies={updateMovies} />
        </div>

    useEffect(() => { fetchMovies('all') }, [])

    return (
        <div className="App">
            <h1 id="tabelLabel">Movie library</h1>
            <p>This is a movie library.</p>
            {contents}
        </div>
    );
}
export default App;